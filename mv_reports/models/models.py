# -*- coding: utf-8 -*-

from odoo import models, fields, api, SUPERUSER_ID, _
from datetime import datetime
from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT

class SaleOrder(models.Model):
    _inherit = 'sale.order'

    memo = fields.Text(string='Memo')
    delivery_carrier_address = fields.Many2one('delivery.carrier.address', string='Carrier Address')
    ship_date = fields.Date(string='Ship Date')
    po_reference = fields.Char(string='Reference')

    @api.multi
    @api.onchange('carrier_id')
    def filter_addresses(self):
        self.delivery_carrier_address = None
        res = {}
        delivery_carrier_addresses = self.env['delivery.carrier.address'].search([('carrier_id', '=', self.carrier_id.id)]).mapped(
            lambda r: r.carrier_id.id)
        res['domain'] = {'delivery_carrier_address': [('carrier_id', 'in', delivery_carrier_addresses)]}
        return res


class SaleOrderLine(models.Model):
    _inherit = 'sale.order.line'

    default_code = fields.Char('Item Name', related="product_id.default_code", store=False, readonly=False)



class PurchaseOrderLine(models.Model):
    _inherit = 'purchase.order.line'

    default_code = fields.Char('Item Name', related="product_id.default_code", store=False, readonly=False)

    @api.onchange('product_id')
    def onchange_product_id(self):
        result = {}
        if not self.product_id:
            return result

        # Reset date, price and quantity since _onchange_quantity will provide default values
        self.date_planned = datetime.today().strftime(DEFAULT_SERVER_DATETIME_FORMAT)
        self.price_unit = self.product_qty = 0.0
        self.product_uom = self.product_id.uom_po_id or self.product_id.uom_id
        result['domain'] = {'product_uom': [('category_id', '=', self.product_id.uom_id.category_id.id)]}

        product_lang = self.product_id.with_context(
            lang=self.partner_id.lang,
            partner_id=self.partner_id.id,
        )
        self.name = product_lang.name
        # if product_lang.description_purchase:
        #     self.name += '\n' + product_lang.description_purchase

        fpos = self.order_id.fiscal_position_id
        if self.env.uid == SUPERUSER_ID:
            company_id = self.env.user.company_id.id
            self.taxes_id = fpos.map_tax(self.product_id.supplier_taxes_id.filtered(lambda r: r.company_id.id == company_id))
        else:
            self.taxes_id = fpos.map_tax(self.product_id.supplier_taxes_id)

        self._suggest_quantity()
        self._onchange_quantity()

        return result

class AccountInvoice(models.Model):
    _inherit = "account.invoice"
    memo = fields.Text(string='Memo', compute='_get_memo', store=False)
    carrier = fields.Char('Name', compute='_get_carrier', store=False)
    carrier_address = fields.Char('Name', compute='_get_carrier_address', store=False)
    sale_order = None

    def _get_memo(self):
        for record in self:
            if record.type == 'out_invoice':
                if not self.sale_order:
                    self.sale_order = self.env['sale.order'].search([('name', '=', self.origin)])
                record.memo = self.sale_order.memo
            else:
                record.memo = None

    def _get_carrier(self):
        if self.type == 'out_invoice':
            if not self.sale_order:
                self.sale_order = self.env['sale.order'].search([('name', '=', self.origin)])
            self.carrier = self.sale_order.carrier_id.name
        else:
            self.carrier = None

    def _get_carrier_address(self):
        if self.type == 'out_invoice':
            if not self.sale_order:
                self.sale_order = self.env['sale.order'].search([('name', '=', self.origin)])
            self.carrier_address = self.sale_order.delivery_carrier_address.name
        else:
            self.carrier_address = None


class AccountInvoiceLine(models.Model):
    _inherit = "account.invoice.line"

    name = fields.Char(string='Description', related="product_id.name", store=True)
    default_code = fields.Char('Item Name', related="product_id.default_code", store=False, readonly=False)

    def _get_invoice_line_name_from_product(self):
        """ Returns the automatic name to give to the invoice line depending on
        the product it is linked to.
        """
        self.ensure_one()
        if not self.product_id:
            return ''
        invoice_type = self.invoice_id.type
        rslt = self.product_id.name
        if invoice_type in ('in_invoice', 'in_refund'):
            if self.product_id.description_purchase:
                rslt += '\n' + self.product_id.description_purchase
        else:
            if self.product_id.description_sale:
                rslt += '\n' + self.product_id.description_sale

        return rslt


class DeliveryCarrier(models.Model):
    _inherit = 'delivery.carrier'

    address = fields.One2many('delivery.carrier.address', 'carrier_id',  string='Addresses')

    @api.model
    def create(self, values):
        if 'product_id' not in values or not values['product_id']:
            category_id = self.env['product.category'].search([('name', '=', 'Deliveries')]).id
            product_template = self.env['product.template'].create({
                'name': values['name'],
                'type': 'service',
                'sale_ok': False,
                'purchase_ok': False,
                'tracking': 'none',
                'categ_id': category_id,
                'product_variant_ids': [(0, 0, {'name': values['name']})]
            })
            values['product_id'] = product_template.product_variant_ids[0].id
        res = super(DeliveryCarrier, self).create(values)
        return res


class DeliveryCarrierAddress(models.Model):
    _name = 'delivery.carrier.address'

    def _get_name(self, address=None, phone=None):
        if not address:
            address = self.address
        if not phone:
            phone = self.phone
        return"{} / Phone: {}".format(address, phone)

    name = fields.Char('Name')
    carrier_id = fields.Many2one('delivery.carrier', 'carrier_id')
    phone = fields.Char('Phone')
    address = fields.Text('Address')
    product_id = fields.Many2one('product.product', string='Delivery Product', required=False, ondelete='restrict')

    @api.model
    def create(self, values):
        print(values, self.address, self.phone, '* '*100)
        values.update({'name': self._get_name(values['address'], values['phone'])})
        return super(DeliveryCarrierAddress, self).create(values)

    @api.multi
    def write(self, values):
        deliver_carrier_address = super(DeliveryCarrierAddress, self)
        deliver_carrier_address.write(values)
        values.update({'name': self._get_name()})
        deliver_carrier_address.write(values)
        return deliver_carrier_address
