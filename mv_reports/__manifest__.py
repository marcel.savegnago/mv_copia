# -*- coding: utf-8 -*-
{
    'name': "Reportes MV",

    'summary': """
        Report's modifications for MV""",

    'description': """
        Report's modifications for MV
    """,

    'author': "Xetechs S.A",
    'website': "http://www.xetechs.com",

    'category': 'Uncategorized',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['base', 'sale', 'purchase', 'delivery'],

    # always loaded
    'data': [
        # 'security/ir.model.access.csv',
        'views/views.xml',
        'views/templates.xml',
    ]
}