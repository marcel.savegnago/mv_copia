# -*- coding: utf-8 -*-
{
    'name': "purchase_mv",
    'summary': """
        Custom modifications for MVCommunications""",
    'description': """
        Custom modifications for MVCommunications
    """,
    'author': "Xetechs",
    'website': "http://www.xetechs.com",
    'category': 'purchase',
    'version': '0.1',
    'depends': ['base'],
    'data': [
        # 'security/ir.model.access.csv',
        # 'views/views.xml'
    ]
}