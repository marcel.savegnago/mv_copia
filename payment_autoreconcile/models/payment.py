# -*- coding: utf-8 -*-
##############################################################################
#
#    Odoo, Luis Aquino -- Xetechs, S.A.
#    Copyright (C) 2019-Today Xetechs (<https://www.xetechs.com>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from odoo import api, exceptions, fields, models, _
from odoo.exceptions import AccessError, UserError, RedirectWarning, ValidationError, Warning


class AccountPayment(models.Model):
    _inherit ="account.payment"

    purchase_id = fields.Many2one('purchase.order', 'Purchase', required=False, help="Purchase order")
    sale_id = fields.Many2one('sale.order', 'Sale Order', reqruired=False, help="Sale order")
    invoice_id = fields.Many2one('account.invoice', 'Customer invoice', required=False, help="Invoice")
    customer_invoice_id = fields.Many2one('account.invoice', 'Customer invoice', required=False, help="Customer invoice")
    vendor_bill_id = fields.Many2one('account.invoice', 'Vendor Bill', required=False, help="Vendor Bill")

    @api.multi
    @api.onchange('partner_id')
    def onchange_partner(self):
        res = {}
        res['domain'] = {'purchase_id': [('partner_id.id', '=', self.partner_id.id)],
                         'sale_id': [('partner_id.id', '=', self.partner_id.id)]}
        return res

    @api.onchange('purchase_id', 'sale_id')
    def onchange_payment(self):
        res = {}
        invoices_ids = []
        if self.payment_type == 'inbound':
            if self.sale_id:
                invoices_ids = []
                self.partner_id = self.sale_id.partner_id
                for inv in self.sale_id.invoice_ids:
                    invoices_ids.append(inv.id)
                res['domain'] = {'invoice_id': [('id', 'in', invoices_ids), ('state', '=', 'open')]}
        elif self.payment_type == 'outbound':
            if self.purchase_id:
                invoices_ids = []
                self.partner_id = self.purchase_id.partner_id
                for inv in self.purchase_id.invoice_ids:
                    invoices_ids.append(inv.id)
                res['domain'] = {'invoice_id': [('id', 'in', invoices_ids), ('state', '=', 'open')]}
        return res

    @api.onchange('invoice_id')
    def onchange_invoice(self):
        self.amount = self.invoice_id.amount_total

    @api.multi
    def post(self):
        for rec in self:
            if rec.payment_type == 'outbound':
                if rec.invoice_id:
                    rec.write({'invoice_ids' : [(4, rec.invoice_id.id, None)]})
            elif rec.payment_type == 'inbound':    
                if rec.invoice_id:
                    rec.write({'invoice_ids' : [(4, rec.invoice_id.id, None)]})
        res = super(AccountPayment, self).post()
        return res
AccountPayment()