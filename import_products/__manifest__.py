# -*- coding: utf-8 -*-

{
    'name': 'Import Products and Variants with Stock',
    'author': 'Wizard Technolab',
    'description': """
Import Products
====================================

This module Import Products And Variants With Stock.
    """,
    'summary': """Import Products And Variants With Stock""",
    'version': '12.0.1.0',
    'price': 20.00,
    "currency": "EUR",
    "support": "wizardtechnolab@gmail.com",
    'category': 'Product',
    'license': 'LGPL-3',
    'sequence': 1,
    'description': """
This module allows to Import Product and Variants with Qty.
=========================================================================
    """,
    'depends': ['stock'],
    'images': ['images/scree_image.png'],
    'data': [
        'wizard/import_product_view.xml',
     ],
}
