(function() {
    let timer = 0;
    function addComma (event) {
        let currentValue =  $(event.target).val()
        if (!currentValue.endsWith(' ')) {
            $(event.target).val(currentValue + ' ')
        }
    }

    $(document).on('keyup', '.stock_move_form .o_field_text',
        (event) => {
        if (timer) {
            clearTimeout(timer);
        }

        timer = setTimeout(() => {
            addComma(event)
        }, 1000)
    });
})();