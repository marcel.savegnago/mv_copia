# -*- coding: utf-8 -*-
##############################################################################
#
#    Odoo, Luis Aquino -- Xetechs, S.A.
#    Copyright (C) 2019-Today Xetechs (<https://www.xetechs.com>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from odoo import api, exceptions, fields, models, _
import re
from odoo.exceptions import AccessError, UserError, RedirectWarning, ValidationError, Warning


class StockMove(models.Model):
    _inherit ="stock.move"

    is_imei = fields.Boolean('¿Doble IMEI?', required=False, default=False)
    barcode_split = fields.Text('Serial number/Lot', required=False, help="Código de barrar para lote, seriales e IMEIs")
    barcode_split2 = fields.Text('Second serial number/Lot', required=False, help="Segundo código de barrar para lote, seriales e IMEIs")
    part_number_barcode = fields.Char('Part number', help="Part number of the device", related='product_id.barcode')
    part_number = fields.Char('Part number', help="Part number of the device")

    @api.onchange('barcode_split')
    def onchange_barcode(self):
        if self.part_number_barcode and not self.part_number:
            self.part_number = self.part_number_barcode
        if self.barcode_split:
            if self.picking_code == 'incoming':
                return self.purchase_split()
            elif self.picking_code == 'outgoing':
                return self.sale_split()

    @api.onchange('barcode_split2')
    def onchange_barcode2(self):
        if self.barcode_split:
            if self.picking_code == 'incoming':
                return self.purchase_split()
            elif self.picking_code == 'outgoing':
                return self.sale_split()

    def purchase_split(self):
        # avoid empty elements
        lst_data = list(filter(None, re.split(' |,|\|\\n|\\r', self.barcode_split) ))
        if re.search('[a-zA-Z]', lst_data[0]):
            part_number = lst_data[0]
            if self.part_number_barcode:
                if self.part_number_barcode != part_number:
                    warning_mess = {
                        'title': _('Wrong Part number'),
                        'message': 'This master code contains a different part number from the one registered'
                    }
                    return {'warning': warning_mess}
                self.part_number = self.part_number_barcode
            elif lst_data[0]:
                self.part_number = lst_data[0]
            # self.product_id.write({'barcode': part_number})

        # always remove others part number
        lst_data = list(filter(lambda x: re.search('^[0-9]*$', x), lst_data))
        if self.part_number:
            self.barcode_split = self.barcode_split.replace(self.part_number, '').strip(', |')

        lst_data2 = []
        double_imei = False
        if len(lst_data) == 2 * len(self.move_line_ids):
            double_imei = True
        if self.barcode_split2:
            lst_data2 = list(filter(None, re.split(' |,|\|', self.barcode_split2) ))
            lst_data2 = list(filter(lambda x: re.search('^[0-9]*$', x), lst_data2))
            self.barcode_split2 = self.barcode_split2.replace(self.part_number, '').strip(', |')
            double_imei = True

        # raise UserError(_("%s")%(lst_data))
        if lst_data:
            # check the quantity of serials
            warning = False
            if double_imei:
                if len(lst_data2) == 0 and len(lst_data) != 2 * len(self.move_line_ids):
                    warning = True
                    if len(lst_data) > 2 * len(self.move_line_ids):
                        warning_mess = {
                            'title': _('More serial/lot number!'),
                            'message': 'More serial/lot number than items, you must enter the data corresponding to the '
                                       'quantity of products to be submitted (serials: {} / Items: {})'.
                                format(len(lst_data)/2, len(self.move_line_ids))
                        }
                    else:
                        warning_mess = {
                            'title': _('Incomplete serial/lot number!'),
                            'message': 'Incomplete serial/lot number, you must enter the data corresponding to the '
                                       'quantity of products to be submitted (serials: {} / Items: {})'.
                                format(len(lst_data)/2, len(self.move_line_ids))
                        }
                elif len(lst_data2) > 0 and len(lst_data) + len(lst_data2) != 2 * len(self.move_line_ids):
                    warning = True
                    if len(lst_data) + len(lst_data2) > 2 * len(self.move_line_ids):
                        warning_mess = {
                            'title': _('More serial/lot number!'),
                            'message': 'More serial/lot number than items, you must enter the data corresponding to the '
                                       'quantity of products to be submitted (serials: {} / serials second: {} / Items: {})'.
                                format(len(lst_data), len(lst_data2), len(self.move_line_ids))
                        }
                    else:
                        warning_mess = {
                            'title': _('Incomplete serial/lot number!'),
                            'message': 'Incomplete serial/lot number, you must enter the data corresponding to the '
                                       'quantity of products to be submitted (serials: {} serials / second: {} / Items: {})'.
                                format(len(lst_data), len(lst_data2), len(self.move_line_ids))
                        }
                else:
                    warning = False
            else:
                if len(lst_data) != len(self.move_line_ids):
                    warning = True
                    if len(lst_data) > len(self.move_line_ids):
                        warning_mess = {
                            'title': _('More serial/lot number!'),
                            'message': 'More serial/lot number than items, you must enter the data corresponding to the '
                                       'quantity of products to be submitted (serials: {} / Items: {})'.
                                format(len(lst_data), len(self.move_line_ids))
                        }
                    else:
                        warning_mess = {
                            'title': _('Incomplete serial/lot number!'),
                            'message': 'Incomplete serial/lot number, you must enter the data corresponding to the '
                                       'quantity of products to be submitted (serials: {} / Items: {})'.
                                format(len(lst_data), len(self.move_line_ids))
                        }
            if warning:
                return {'warning': warning_mess}

            if double_imei:
                if len(lst_data2) >= 1:
                    item = 0
                    for line in self.move_line_ids:
                        try:
                            line.lot_name = "IMEI 1:%s / IMEI 2:%s" % (str(lst_data[item]), str(lst_data2[item]))
                        except IndexError:
                            warning_mess = {
                                'title': _('Incomplete serial/lot number!'),
                                'message': 'Incomplete serial/lot number, you must enter the data corresponding to the '
                                           'quantity of products to be submitted'
                            }
                            return {'warning': warning_mess}
                        line.qty_done = 1.00
                        line.write({'lot1': str(lst_data[item]), 'lot2': str(lst_data2[item])})
                        item += 1
                else:
                    item = 0
                    for line in self.move_line_ids:
                        try:
                            line.lot_name = "IMEI 1:%s / IMEI 2:%s" % (str(lst_data[item]), str(lst_data[item + 1]))
                        except IndexError:
                            warning_mess = {
                                'title': _('Incomplete serial/lot number!'),
                                'message': 'Incomplete serial/lot number, you must enter the data corresponding to the '
                                           'quantity of products to be submitted'
                            }
                            return {'warning': warning_mess}
                        line.qty_done = 1.00
                        line.write({'lot1': str(lst_data[item]), 'lot2': str(lst_data[item + 1])})
                        item += 2
            else:
                item = 0
                for line in self.move_line_ids:
                    # item += 1
                    try:
                        line.lot_name = str(lst_data[item])
                    except IndexError:
                        warning_mess = {
                            'title': _('Incomplete serial/lot number!'),
                            'message': 'Incomplete serial/lot number, you must enter the data corresponding to the '
                                       'quantity of products to be submitted'
                        }
                        return {'warning': warning_mess}

                    line.qty_done = 1.00
                    item += 1

    def sale_split(self):
        # avoid empty elements
        lst_data = list(filter(None, re.split(' |,|\|\\n|\\r', self.barcode_split)))
        if re.search('[a-zA-Z]', lst_data[0]):
            if self.part_number_barcode:
                if self.part_number_barcode != lst_data[0]:
                    warning_mess = {
                        'title': _('Wrong Part number'),
                        'message': 'This master code contains a different part number from the one registered'
                    }
                    return {'warning': warning_mess}
                self.barcode_split = self.barcode_split.replace(self.part_number, '').strip(', |')
        # always remove others part number
        lst_data = list(filter(lambda x: re.search('^[0-9]*$', x), lst_data))

        lst_data2 = []
        double_imei = False
        if len(lst_data) == 2 * len(self.move_line_ids):
            double_imei = True
        if self.barcode_split2:
            lst_data2 = list(filter(None, re.split(' |,|\|', self.barcode_split2)))
            lst_data2 = list(filter(lambda x: re.search('^[0-9]*$', x), lst_data2))
            self.barcode_split2 = self.barcode_split2.replace(self.part_number, '').strip(', |')
            double_imei = True

        if lst_data:
            # check the quantity of serials
            warning = False
            if double_imei:
                if len(lst_data2) == 0 and len(lst_data) != 2 * len(self.move_line_ids):
                    warning = True
                    if len(lst_data) > 2 * len(self.move_line_ids):
                        warning_mess = {
                            'title': _('More serial/lot number!'),
                            'message': 'More serial/lot number than items, you must enter the data corresponding to the '
                                       'quantity of products to be submitted (serials: {} / Items: {})'.
                                format(len(lst_data) / 2, len(self.move_line_ids))
                        }
                    else:
                        warning_mess = {
                            'title': _('Incomplete serial/lot number!'),
                            'message': 'Incomplete serial/lot number, you must enter the data corresponding to the '
                                       'quantity of products to be submitted (serials: {} / Items: {})'.
                                format(len(lst_data) / 2, len(self.move_line_ids))
                        }
                elif len(lst_data2) > 0 and len(lst_data) + len(lst_data2) != 2 * len(self.move_line_ids):
                    warning = True
                    if len(lst_data) + len(lst_data2) > 2 * len(self.move_line_ids):
                        warning_mess = {
                            'title': _('More serial/lot number!'),
                            'message': 'More serial/lot number than items, you must enter the data corresponding to the '
                                       'quantity of products to be submitted (serials: {} / serials second: {} / Items: {})'.
                                format(len(lst_data), len(lst_data2), len(self.move_line_ids))
                        }
                    else:
                        warning_mess = {
                            'title': _('Incomplete serial/lot number!'),
                            'message': 'Incomplete serial/lot number, you must enter the data corresponding to the '
                                       'quantity of products to be submitted (serials: {} serials / second: {} / Items: {})'.
                                format(len(lst_data), len(lst_data2), len(self.move_line_ids))
                        }
                else:
                    warning = False
            else:
                if len(lst_data) != len(self.move_line_ids):
                    warning = True
                    if len(lst_data) > len(self.move_line_ids):
                        warning_mess = {
                            'title': _('More serial/lot number!'),
                            'message': 'More serial/lot number than items, you must enter the data corresponding to the '
                                       'quantity of products to be submitted (serials: {} / Items: {})'.
                                format(len(lst_data), len(self.move_line_ids))
                        }
                    else:
                        warning_mess = {
                            'title': _('Incomplete serial/lot number!'),
                            'message': 'Incomplete serial/lot number, you must enter the data corresponding to the '
                                       'quantity of products to be submitted (serials: {} / Items: {})'.
                                format(len(lst_data), len(self.move_line_ids))
                        }
            if warning:
                return {'warning': warning_mess}

            if double_imei:
                lst_data_double = []
                lot1_data = []
                lot2_data = []
                if len(lst_data2) >= 1:
                    for item in range(int(len(lst_data))):
                        lst_data_double.append("IMEI 1:{} / IMEI 2:{}".format(lst_data[item], lst_data2[item]))
                    lot1_data = lst_data
                    lot2_data = lst_data2
                    lst_data = lst_data_double

                else:
                    for item in range(int(len(lst_data) / 2)):
                        lst_data_double.append("IMEI 1:{} / IMEI 2:{}".format(lst_data[item * 2], lst_data[item * 2 + 1]))
                        lot1_data.append(lst_data[item * 2])
                        lot2_data.append(lst_data[item * 2 + 1])
                    lst_data = lst_data_double

            move_line_ids = []
            lots = []
            for serial in lst_data:
                lot = self.env["stock.production.lot"].search([('name', '=', serial)])
                move_line = self.env["stock.move.line"].search([('lot_name', '=', serial)])
                if not lot.id:
                    warning_mess = {
                        'title': _('Serial/lot number not found!'),
                        'message': 'The serial/lot number "{}" can not be found!, you must enter a valid '
                                   'serial/lot number'.format(serial)
                    }
                    return {'warning': warning_mess}
                else:
                    lots.append(lot)
                    move_line_ids.append(move_line)

            
            for index, (line, lot) in enumerate(zip(self.move_line_ids, lots)):
                self.move_line_ids[index].lot_id = lot.id
                self.move_line_ids[index].lot_name = lot.name
                data = {'qty_done': 1.00, 'product_uom_qty': 1.00, 'lot_id': lot.id, 'lot_name': lot.name}
                if double_imei:
                    self.move_line_ids[index].write({'lot1': int(lot1_data[index]), 'lot2': int(lot2_data[index])})
                line.update(data)

    # @api.model
    # def create(self, values):
    #     # if self.part_number:
    #     #     self.product_id.write({'barcode': selfpart_number})
    #     print(values, '* '*100)
    #     # values.update({'name': self._get_name()})
    #     return super(StockMove, self).create(values)

    # @api.multi
    # def write(self, values):
    #     print(values, '* '*100 )
    #     stock_move = super(StockMove, self)
    #     # deliver_carrier_address.write(values)
    #     # values.update({'name': self._get_name()})
    #     # deliver_carrier_address.write(values)
    #     return stock_move



StockMove()

class StockPicking(models.Model):
    _inherit = "stock.picking"

    @api.multi
    def button_validate(self):
        for line in self.move_ids_without_package:
            line.product_id.write({'barcode': line.part_number})
        return super(StockPicking, self).button_validate()
StockPicking()


class StockMoveLine(models.Model):
    _inherit = "stock.move.line"
    lot1 = fields.Char('Imei 1')
    lot2 = fields.Char('Imei 2')
