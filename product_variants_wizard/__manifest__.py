# -*- coding: utf-8 -*-
{
    'name': "Product Variants Wizard",
    'description': """
        Allows to register products (variants) in a simple way
    """,
    'author': "Xetechs",
    'website': "http://www.xetechs.com",
    'category': 'purchase',
    'version': '0.1',
    'depends': ['purchase','alan_customize'],
    'data': [
        # 'security/ir.model.access.csv',
        'views/product_variants_wizard_views.xml',
        'views/product_views.xml',
        'views/product_tree_views.xml',
        'views/product_assets.xml',
        'views/purchase_order_views.xml',
        'views/purchase_requisition_views.xml',
        'views/product_barcode_wizard.xml',
        'views/product_template_views.xml'
    ],
}