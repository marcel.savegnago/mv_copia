# -*- coding: utf-8 -*-

from odoo import models, fields, api, _
from odoo.exceptions import UserError, ValidationError


class ProductVariantsWizard(models.TransientModel):
    _name = 'product.variants_wizard'
    _description = 'Product Variants Wizard'
    _rec_name = "product_tmpl_id"

    def get_cellphone_cat(self):
        return self.env['product.category'].search([('name', '=', 'Cellular Phones')]).id

    product_brand_id = fields.Many2one('product.brand', 'Brand')
    product_tmpl_id = fields.Many2one('product.template', string="Display Name")
    product_tmpl_name = fields.Char('Product name', related='product_tmpl_id.name')
    category_id = fields.Many2one('product.category', 'Category', default=get_cellphone_cat)
    line_ids = fields.One2many('product.variants_wizard.lines', 'wizard_id', 'Lines')
    is_warehouse = fields.Boolean('Warehouse', default=False)
    product_variants_ids = fields.One2many('product.variants.lines', 'wizard_id', 'Variants')
    product_name = fields.Char('Hidden product field')
    new_product = fields.Boolean('New product', default=False)
    current_variants = fields.One2many(related='product_tmpl_id.product_variant_ids', string='Current Products')

    @api.multi
    def create_variants(self):
        if len(self.line_ids) == 0:
            message = 'You have not added products in the form, please verify that you added items in the product form'
            raise UserError(_(message))
        
        # Create the product if needed
        if not self.product_tmpl_id.id:
            product_template = self.create_product()
            self.new_product = True
            self.product_tmpl_id = product_template.id
            # Create the variant if not exists
        values = self.line_ids. \
            filtered(lambda r: len(self.env["product.product"].search(
            [
                ('name', '=', r.description_sale),
                ('default_code', '=', r.internal_reference),
                ('product_tmpl_id', '=', self.product_tmpl_id.id)
            ])) == 0). \
            mapped(lambda r: (0, 0, {'name': r.description_sale,
                                     'default_code': r.internal_reference,
                                     }))
        self.product_tmpl_id.update({'product_variant_ids': values, 'product_brand_id': self.product_brand_id.id})

        # Clean variants without internal_reference and description_sale
        products = self.env["product.product"].search([
            ('name', '=', None),
            ('default_code', '=', None),
            ('product_tmpl_id', '=', self.product_tmpl_id.id)
        ])
        products.unlink()

        return {
            "type": "ir.actions.act_window",
            "res_model": "product.variants_wizard",
            "views": [[False, "form"]],
            "res_id": self.id,
            "target": "main",
            "context": {'show_message': True, 'new_product': self.new_product},
        }

    def create_product(self, values=None):
        if values:
            product_name = values['product_name']
            category_id = values['category_id']
            category = self.env['product.category'].search([('id', '=', category_id)])
            category_name = category.name
            brand_id = values['product_brand_id']
        else:
            product_name = self.product_name
            category_id = self.category_id.id
            category_name = self.category_id.name
            brand_id = self.product_brand_id.id

        # Check if product template exists
        if product_name:
            product_template = self.env["product.template"].search([('name', '=', product_name)])
            if not product_template.id:
                tracking = 'none'
                if category_name in ['Cellular Phones', 'Tablets']:
                    tracking = 'serial'

                product_template = self.env['product.template'].create({
                    'name': product_name,
                    'tracking': tracking,
                    'product_brand_id': brand_id,
                    'categ_id': category_id
                })

                return product_template

    @api.model
    def create(self, values):
        product_template = None
        if not values['product_tmpl_id']:
            values['new_product'] = True
            product_template = self.create_product(values)
        if product_template:
            values['product_tmpl_id'] = product_template.id
        res = super(ProductVariantsWizard, self).create(values)
        return res


class ProductVariantsWizardLines(models.TransientModel):
    _name = 'product.variants_wizard.lines'
    _description = 'Product Variants Wizard Lines'

    internal_reference = fields.Char('Item Name')
    description_sale = fields.Char('Description')
    wizard_id = fields.Many2one('product.variants_wizard')


class ProductVariantsBarcode(models.TransientModel):
    _name = 'product.variants.barcode'
    _description = 'Product Variants Barcode'
    _rec_name = "product_tmpl_id"

    product_brand_id = fields.Many2one('product.brand', 'Brand')
    product_tmpl_id = fields.Many2one('product.template', string="Display Name")
    is_warehouse = fields.Boolean('Warehouse', default=False)
    product_variants_ids = fields.One2many('product.variants.lines', 'wizard_id', 'Variants')

    @api.onchange('product_tmpl_id')
    def onchange_product_template(self):
        # First clean the variants lines
        self.product_variants_ids = None
        product_ids = []
        if self.product_tmpl_id:
            if self.is_warehouse:
                for variant in self.product_tmpl_id.product_variant_ids:
                    var = {
                        'barcode': "",
                        'product_variant_id': variant.id or False,
                    }
                    product_ids.append((0, 0, var))
        self.update({
            'product_variants_ids': product_ids,
        })

    @api.multi
    def action_asign_barcode(self):
        if self.product_variants_ids:
            for line in self.product_variants_ids:
                line.product_variant_id.write({'barcode': line.barcode})
        return True


class ProductVariantsLines(models.TransientModel):
    _name = "product.variants.lines"
    _description = "Variantes ya registradas"

    product_variant_id = fields.Many2one('product.product', 'Variants', required=False, readonly=False)
    description = fields.Char('Description', related='product_variant_id.name')
    item_name = fields.Char('Item Name', related='product_variant_id.default_code')
    barcode = fields.Char('Barcode')
    wizard_id = fields.Many2one('product.variants.barcode')


ProductVariantsLines()
